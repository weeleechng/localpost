<?php

//http_response_code (500); return;

if (isset ($_GET['mode']))
{
  switch ($_GET['mode'])
  {
    case "verifyrequest":
      $respond = array (
        'status' => "success",
        'reason' => "Internal Server Success",
        'session' => md5 (uniqid (rand(), true)),
      );
      break;

    case "verifysubmit":
      $respond = array (
        'status' => "success",
        'reason' => "Verification code correct",
      );
      break;

    default:
      break;
  }
}
else
{
  $respond = array (
    'post' => $_POST,
    'files' => $_FILES,
  );
}

print json_encode ($respond);

?>